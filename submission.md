#### QUESTIONS
									###Dakota Smith

Questions:								Answers:
####Inspect Multiboot Image
1) What kind of file did you just download?				I downloaded an torrent of the image of the PinePhone Multi-Distro Demo.
2) Why do we call it an image?						It is called an image as it is the exact replica of the computer at the time the image was taken.
3) Why would Megi use this format to distribute it?			Megi would use an image so that everyone who downloads it gets the exact same thing. The torrent is used to speed up dowloading, as it is a large file.
####Hashes
1) How do the output of the last two commands compare?			the cat SHA256 command displays two SHA keys, one for multi.img.gz and a different one for multi.img. The sha256sum command directly displays the SHA key 
									for multi.img.gz only
2) Why would MEGI distribute the SHA256 file with the image.		They would distribute this file as it allows you to view the SHA key for the multi.img file directly.
####Look at the Mulitboot image
1) WHy does it look funny?						It looks funny because it is encoded and scrambled. When using head to look at this .md file, head directly pulls up the contents of this file in a preview.
#### Extract the actual image from what you downloaded
1) What does 'zcat' mean						Zcat uncompressees a list of files on the command line and writes the uncompressed data to standard output. [Source](https://linux.die.net/man/1/zcat)
2) What happens after running this command?				The file is uncompressed and written to standard output as multi.img
3) what do the contents of the new file look like as compared to the 
*.gz version?								The contents of the new file are viewable on my device.
#### Deeper inspection of the file
1) what do we know about the decompressed file multi.img ?		We know its size, IO size, disk identifier, etc.
2) what does it contain?						It contains 9.78 GiB of data.
3) how did the output of the sha256sum command compare to the info
in the SHA256 file?							The output is different from the SHA256 string from the info file.

NOTE: Was using WSL2. Unable to use head command and WSL is unable to run loop devices, therefore unable to do loop device portion of Homework. Will be setting up Virtual Machine and finishing homework at a later time.
